#!/bin/bash

set -euo pipefail

if [[ $XDG_SESSION_TYPE == "x11" ]]; then
    xclip -selection clipboard -out $@
else
    wl-paste $@
fi
