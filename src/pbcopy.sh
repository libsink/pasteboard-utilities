#!/bin/bash

set -euo pipefail

if [[ $XDG_SESSION_TYPE == "x11" ]]; then
    xclip -selection clipboard -in $@
else
    wl-copy $@
fi
