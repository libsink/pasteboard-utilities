build: src/pbcopy.sh src/pbpaste.sh
	mkdir bin
	install src/pbcopy.sh bin/pbcopy --mode=555
	install src/pbpaste.sh bin/pbpaste --mode=555

install: bin/pbcopy bin/pbpaste
	install bin/pbcopy /usr/local/bin/pbcopy
	install bin/pbpaste /usr/local/bin/pbpaste

check:
	echo -n "✅ Copy and Paste successful" | bin/pbcopy
	bin/pbpaste
	bin/pbcopy -h
	bin/pbcopy -v
	bin/pbpaste -h
	bin/pbpaste -v

clean:
	$(RM) -r bin
